## TO USE FLASK AND ANSIBLE TOWER TO LAUNCH WINDOWS INSTANCE AND INSTALL SOFTWARES OVER THE FLY


#PROJECT STRUCTURE :

```
azurewindows_ansible_vm
├── README.md
├── WebUiLogger
│   ├── LICENSE
│   ├── README.md
│   ├── dump.rdb
│   ├── install.sh
│   ├── logmon
│   │   ├── __init__.py
│   │   ├── __init__.pyc
│   │   ├── config.py
│   │   ├── config.pyc
│   │   ├── static
│   │   │   ├── css
│   │   │   │   └── style.css
│   │   │   ├── images
│   │   │   │   └── overlay.png
│   │   │   └── js
│   │   │       └── jquery.min.js
│   │   ├── templates
│   │   │   └── index.html
│   │   ├── views.py
│   │   └── views.pyc
│   ├── requirement.txt
│   └── runner.py
├── Webui_launcher
│   ├── 1
│   ├── LICENSE
│   ├── README.md
│   ├── Resources
│   │   ├── WTForms-field-types.csv
│   │   └── WTForms-validators.csv
│   ├── data.pyc
│   ├── requirements.txt
│   ├── static
│   │   └── css
│   │       └── main.css
│   ├── templates
│   │   ├── 404.html
│   │   ├── 500.html
│   │   ├── index
│   │   ├── index.html
│   │   ├── soft
│   │   └── soft.html
│   └── web.py
├── azure-ansible-playbooks
│   ├── LICENSE.md
│   ├── README.md
│   ├── SIG_generalized_image
│   │   ├── 00-prerequisites.yml
│   │   ├── 01a-create-generalized-image.yml
│   │   ├── 02-create-shared-image-gallery.yml
│   │   ├── 03a-create-shared-image-generalized.yml
│   │   ├── 04a-create-vm-using-generalized-image.yml
│   │   ├── 05a-create-vmss-using-generalized-image.yml
│   │   ├── 06-get-info.yml
│   │   ├── 07-delete-gallery.yml
│   │   └── vars.yml
│   ├── __helpers
│   │   └── __delete_test_resource_group.yml
│   ├── ansible.log
│   ├── azure_create_windows_vm.retry
│   ├── azure_create_windows_vm.yml
│   ├── azure_rm_subnet
│   │   ├── sub_with_nsg_in_other_rg1.yml
│   │   ├── sub_with_nsg_in_other_rg2.yml
│   │   └── subnet_with_nsg.yml
│   ├── create.sh
│   ├── logs
│   │   ├── ansible.log
│   │   └── ansible1.log
│   ├── networkinterface_with_virtualnetwork_in_another_resource_group.yml
│   ├── requirement
│   ├── requirement.txt
│   ├── rest
│   │   ├── eventhub-create.yml
│   │   ├── resourcegroup_dump_resources.yml
│   │   └── sql-managed-instance.yml
│   ├── vm_create.yml
│   ├── vmss
│   │   ├── ansible.cfg
│   │   ├── get-hosts-tasks.yml
│   │   ├── vmss-autoscale.yml
│   │   ├── vmss-create.yml
│   │   ├── vmss-ping.yml
│   │   ├── vmss-scale-out.yml
│   │   ├── vmss-setup-deploy.yml
│   │   └── vmss-test-url.yml
│   └── vmss_extension
│       ├── 01-create-vmss.yml
│       ├── 02-setup-httpd.yml
│       └── 03-update-instances.yml
└── playbook_windows
    ├── README.md
    ├── install.sh
    ├── inventory
    │   ├── FinalP1572039129
    │   ├── README.md
    │   ├── asdadsd1572184010
    │   ├── asdasdasdas1572184378
    │   ├── devops1572031433
    │   ├── devops1572031511
    │   ├── hosts
    │   ├── motf11572035717
    │   └── motf11572035804
    ├── readme.txt
    ├── requirement.txt
    ├── roles
    │   ├── win_chocolatey_install
    │   │   ├── files
    │   │   │   └── install.ps1
    │   │   └── tasks
    │   │       └── main.yml
    │   ├── win_chocolatey_setting
    │   │   └── tasks
    │   │       └── main.yml
    │   └── windows-openssh
    │       ├── defaults
    │       │   └── main.yml
    │       ├── handlers
    │       │   └── main.yml
    │       ├── tasks
    │       │   ├── install.yml
    │       │   └── main.yml
    │       ├── templates
    │       │   ├── install-openssh.ps1.j2
    │       │   └── sshd_config.j2
    │       └── vars
    │           └── main.yml
    ├── win_choco.yml
    └── winrm_installer.ps1
```




#Case Study

Please provide an outline and related ansible playbook, or other scripts, to do the following:

1. Configure a basic (and brand new) Windows 10 Enterprise system for Ansible access
2. Install Chocolatey Windows Package Manager using Ansible
3. Configure and enable SSH on standard port so that it starts up on restart of machine
4. Upload a configurable SSH key (I should be able to set my key so that I can test this process)
5. Install Git using Chocolatey


 TASK 1

1. Configure a basic (and brand new) Windows 10 Enterprise system for Ansible access
SOLUTION:

SUMMARY :  

I have used Python Flask as a frond end Application that allow you to deploy Windows 10 Enterprise over the fly without any manual intervation. The form allow you to give your machine Name,Username and Admin Password that will allow you to do RDP. The front end invokes the Ansible Playbook in backend Named ` azure_create_windows_vm.yml`  with the required roles ,sizing capacity,region and with public access. As a result the Ansible playbook make sures to launch the new instance with all required parameters and prerequisite to do SSH as well as WINRM protocol

Tools & SOFTWARE Used :

1.	PYTHON FLASK
2.	ANSIBLE PLAYBOOK
3.	AZURE CLOUD
4.	WINDOWS 10 ENTERPRISE
5.	BASH
6.	PYTHON MODULES (WINRM,GALAXY PLAYBOOK)


Prerequisites :

1.	You should have valid Azure account
2.	Python3  or 2.7 Installed in your computer
3.	Ansible Installed
4.	Python Pip
5.	Windows 10 Enterprise Availabilty
6.	PYTHON MODULES (WINRM,GALAXY PLAYBOOK)

Windows 10 Details :

1.	os_type: Windows
2.	offer: Windows-10
3.	publisher: MicrosoftWindowsDesktop
4.	sku: rs1-enterprise
5.	version: latest

```
REPOSITORY :
LOGGER FLASK  : https://bitbucket.org/pankaj1991/azurewindows_ansible_vm/src/master/WebUiLogger/
WEb GUI FLASK FLASK : https://bitbucket.org/pankaj1991/azurewindows_ansible_vm/src/master/Webui_launcher/
ANSIBLE PLAYBOOK TO LAUNCH INSTANCE : https://bitbucket.org/pankaj1991/azurewindows_ansible_vm/src/master/azure-ansible-playbooks/
ANSIBLE PLAYBOOK TO INSTALL SOFTWARES INSTANCE : https://bitbucket.org/pankaj1991/azurewindows_ansible_vm/src/master/playbook_windows/
```

INSTALLATION STEPS :

```
git clone https://pankaj1991@bitbucket.org/pankaj1991/azurewindows_ansible_vm.git
```

For starting the FLASK WEB LAUNCHER to Launch the instances using GUI

```
cd Webui_launcher
pip install -r requirement.txt
(If you get error use --upgrade --force-reinstall for dependency)
python web.py
```

Open the browser to launch new Instance

Link to Provision New Instance : `127.0.0.1:5000`
Link to Configure New Instance : `127.0.0.1:5000/soft`




Next Step is to Start Launcher for the LOGS. Assuming you have cloned the repo
```

cd WebUiLogger
pip install -r requirement.txt
(If you get error use --upgrade --force-reinstall for dependency)
```

Install all required dependency like redis-server and start using

`python runner.py`

Opne the browser check LOGS

`127.0.0.1:5001`



INSTALLATION STEPS FOR INSTALLATION of PLAYBOOK :
```
$ cd azure-ansible-playbooks
pip install -r requirement.txt --upgrade --force-reinstall
pip install pywinrm
pip install ansible[azure]
ansible-galaxy install azure.azure_preview_modules
pip install -r ~/.ansible/roles/azure.azure_preview_modules/files/requirements-azure.txt
pip install azure-cli
```

GET AZURE SUBSCRIPTION ID
```
subscription_id=xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
client_id=xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
secret=xxxxxxxxxxxxxxxxx
tenant=xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
```
METHOD TO GET AZURE SUBSCRIPTION ID
```
az login
az account list --output table
Name        CloudName    SubscriptionId                        State    IsDefault
----------  -----------  ------------------------------------  -------  -----------
Free Trial  AzureCloud   d02ff154-077d-4e20-9793-f81346ea8e1a  Enabled  True
sh-3.2# az account list
[
  {
    "cloudName": "AzureCloud",
    "id": "d02ff154-077d-4e20-9793-f81346ea8e1a",
    "isDefault": true,
    "name": "Free Trial",
    "state": "Enabled",
    "tenantId": "803c8238-cb04-46f7-8319-fd3334dd71ab",
    "user": {
      "name": "coolpankaj.1183@gmail.com",
      "type": "user"
    }
  }
]
sh-3.2#  az account set --subscription="d02ff154-077d-4e20-9793-f81346ea8e1a"
sh-3.2# az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/d02ff154-077d-4e20-9793-f81346ea8e1a"
Creating a role assignment under the scope of "/subscriptions/d02ff154-077d-4e20-9793-f81346ea8e1a"
  Retrying role assignment creation: 1/36
  Retrying role assignment creation: 2/36
  Retrying role assignment creation: 3/36
  Retrying role assignment creation: 4/36
{
  "appId": "3df1ad87-a0e0-46e9-8dcd-f35b545125fa",
  "displayName": "azure-cli-2019-10-25-14-41-40",
  "name": "http://azure-cli-2019-10-25-14-41-40",
  "password": "5eb0e608-7d9b-4b6f-bb32-159b289a0b06",
  "tenant": "803c8238-cb04-46f7-8319-fd3334dd71ab"
}

```

appId is the client_id defined above.
password is the client_secret defined above.
tenant is the tenant_id defined above.

```pip install -I azure-nspkg```

This Uses Manual Method to Run Playbook but recommended is flask To Launch VM go to
cd azure-ansible-playbooks;

```cd azure-ansible-playbooks;
ansible-playbook azure_create_windows_vm.yml -e "resource_group_name=test-rg" -  -e "resource_vm_name=[machine name]" -e "resource_username=[username]" -e "resource_password=[password]"
```
After the Instance IS UP go to `cd playbook_windows` To launch the Softwares to Install the Softwares.

Create HOST File in path inventory/

```[azure]
XXXXXX

[azure:vars]
ansible_connection=winrm
ansible_user=XXXX
ansible_password=XXXX
ansible_remote_tmp=%TEMP%
become_method=runas
ansible_winrm_server_cert_validation=ignore
Execute Playbook for Packages
cd /Users/immortal/Documents/Ansible_windows/azurewindows_ansible_vm/azurewindows_ansible_vm/playbook_windows
ansible-playbook -i inventory/hosts win_choco.yml
```

Step 1 :

1.	Python Flask frontend that runs over the port 127.0.0.1:5000/index.html. How to run the commands would be present in the bitbucket. The application allow you to have the capability to provide your Machine Name, Username and Password for launching New Instance



2.	Once you fill in all your details . Make sure the validations are in place . Once prefilled . Click  Lets Deploy



3.	Next step is you need to wait and monitor the logs available on 127.0.0.1:5001. Question would arise why the logs are on separate port and the answer is because we wanted to give user the real time experience keeping in mind that the Windows take a bit longer time to Launch




4.	Now moving on to the backend the backend calls a ansible playbook as described below
```
/usr/local/bin/ansible-playbook   /azure-ansible-playbooks/azure_create_windows_vm.yml -e "resource_group_name=PANKAJ_RG"  -e "resource_vm_name=$1" -e "resource_username=$2" -e "resource_password=$3"
```

$1 = Is the Machine Name sent From Frontend
$2 = Is the Username sent from Frontend
$3 = Is the Password sent from Frontend

Backend Logs
```
********** VM CREATION STARTED FOR motf *******

PLAY [localhost] ***************************************************************

TASK [Gathering Facts] *********************************************************
ok: [localhost]

TASK [Prepare random postfix] **************************************************
ok: [localhost]

PLAY [provision new azure host] ************************************************

TASK [Gathering Facts] *********************************************************
ok: [localhost]

TASK [create Azure resource group] *********************************************
ok: [localhost]

TASK [create Azure virtual network in resource group] **************************
changed: [localhost]

TASK [create Azure subnet in virtualnetwork] ***********************************
changed: [localhost]

TASK [create Azure storage account] ********************************************
changed: [localhost]

TASK [provision new Azure virtual host] ****************************************
changed: [localhost]

TASK [create Azure vm extension to enable HTTPS WinRM listener] ****************


changed: [localhost]

TASK [wait for the WinRM port to come online] **********************************
ok: [localhost]

TASK [azure_rm_securitygroup] **************************************************
changed: [localhost]

PLAY RECAP *********************************************************************
localhost                  : ok=11   changed=6    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
```

5.	Let the process be complete and you will see a success message for the Successful Installation of New Windows Machine on the Cloud and you can do the RDP successfully. You will see a name of the vm with some number that’s randomizer we use so that no one can have the same name of VM








                TASK 2

1. Install Chocolatey Windows Package Manager using Ansible
2. Configure and enable SSH on standard port so that it starts up on restart of machine
3. Upload a configurable SSH key (I should be able to set my key so that I can test this process)
4. Install Git using Chocolatey

#Summary :

As you know I have launched  a new image of the Windows 10 on the azure using our flask application now the next is to get the public ip from azure and put it in our flask application to install the required softwares through Package Manager, Here we used the same approach as asked in the Bonus question where we have a front end flask application that ask you to put the Machine IP , Username and Password in order to start installation using ansible playbook. The front end is on 127.0.0.1:5000/soft.html . The form allows Username,Password,Machine Ip. Once invoked it calls the backend and ansible playbook runs through its inventory host to install the required software . It will also install the ssh with required configuration as well as generate ssh-keygen where later you can go on the machine and download the public to start using through key
Tools & SOFTWARE Used :

7.	PYTHON FLASK
8.	ANSIBLE PLAYBOOK
9.	AZURE CLOUD
10.	WINDOWS 10 ENTERPRISE
11.	BASH
12.	PYTHON MODULES (WINRM,GALAXY PLAYBOOK)


#Prerequisites :

7.	You should have valid Azure account
8.	Python3  or 2.7 Installed in your computer
9.	Ansible Installed
10.	Python Pip
11.	Windows 10 Enterprise Availabilty
12.	PYTHON MODULES (WINRM,GALAXY PLAYBOOK)

#Windows 10 Details :

1. os_type: Windows
2. offer: Windows-10
3. publisher: MicrosoftWindowsDesktop
4. sku: rs1-enterprise

1.	Open the browser 127.0.0.1:5000/soft.html . This is the flask application that will allow you to enter Machine IP, Username and Password. In order to install all asked software through Package Manager.



2.	In the step to when you fill the ip of machine (Azure), Username and Password. We will invoke ansible playbook in background in order to install the softwared through package manager

`/usr/local/bin/ansible-playbook  -i playbook_windows/inventory/$2$t playbook_windows/win_choco.yml`

`$2 – Is the inventory file name consist of all the required vars and it will invoke win_choco.yml which has all the software installation parameters.`



3.	Next step is you need to wait and monitor the logs available on 127.0.0.1:5001. Question would arise why the logs are on separate port and the answer is because we wanted to give user the real time experience keeping in mind that the Windows take a bit longer time to Launch. Where you can see what are the softwares  being installed.




BACKEND LOGS :

```
********** VM INSTALLATION STARTED FOR 40.121.142.56 *******

PLAY [Windows Connection Test] *************************************************

TASK [ping] ********************************************************************
ok: [40.121.142.56]

PLAY [Roles Execution] *********************************************************

TASK [win_chocolatey_install : Install Chocolatey] *****************************
changed: [40.121.142.56]

TASK [win_chocolatey_setting : Install GIT] ************************************
changed: [40.121.142.56]

TASK [win_chocolatey_setting : Install Google Chrome] **************************

changed: [40.121.142.56]

TASK [win_chocolatey_setting : Set the cache location] *************************
changed: [40.121.142.56]

TASK [win_chocolatey_setting : Stop Chocolatey on the first package failure] ***
changed: [40.121.142.56]

TASK [win_chocolatey_setting : Add new internal source] ************************
changed: [40.121.142.56]

TASK [windows-openssh : Create temporary directory] ****************************
changed: [40.121.142.56]

TASK [windows-openssh : Get openssh for windows release] ***********************
changed: [40.121.142.56]

TASK [windows-openssh : Unzip openssh in extraction dir] ***********************
changed: [40.121.142.56]

TASK [windows-openssh : Check if ssh private key exists] ***********************
ok: [40.121.142.56]

TASK [windows-openssh : Copy installation script] ******************************
changed: [40.121.142.56]

TASK [windows-openssh : Run installation script] *******************************
changed: [40.121.142.56]

TASK [windows-openssh : Deploy ssh server configuration] ***********************
changed: [40.121.142.56]

RUNNING HANDLER [windows-openssh : restart sshd] *******************************
changed: [40.121.142.56]

PLAY RECAP *********************************************************************
40.121.142.56              : ok=15   changed=13   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```
4.	Let the process be complete and you will see a success message for the Successful Installation from ansible playbook successfully. Now you can access using ssh or other wise download the pub key from the RDP machine.
