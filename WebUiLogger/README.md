# Ansible Logger

PROJECT STRUCTURE :

```
WebUiLogger
├── LICENSE
├── README.md
├── dump.rdb
├── install.sh
├── logmon
│   ├── __init__.py
│   ├── __init__.pyc
│   ├── config.py
│   ├── config.pyc
│   ├── static
│   │   ├── css
│   │   │   └── style.css
│   │   ├── images
│   │   │   └── overlay.png
│   │   └── js
│   │       └── jquery.min.js
│   ├── templates
│   │   └── index.html
│   ├── views.py
│   └── views.pyc
├── requirement.txt
└── runner.py

6 directories, 16 files
```

It is a realtime log reader written with Flask and Juggernaut.

Prerequisites
1. Redis-Server
2. Python
3. Flask
4. Node
5. NPM

## Installation Tested on Mac

Create the Directory

Start by cloning this repository in /Users/immortal/Documents/Ansible_windows/ Directory:

    $ git clone https://bitbucket.org/pankaj1991/azurewindows_ansible_vm.git

This application does assume you have Node installed so that you can make
use of npm and run Juggernaut. You will need npm to install Juggernaut if not
already installed:

    $ curl http://npmjs.org/install.sh | sh

Now you should be able to install Juggernaut:
```
    $ pip install -r requirement.txt
    $ npm install -g juggernaut
    $ pip install flask
    $ pip install -U gevent
    $ brew install redis-server
```
To keep working start redis-server

Once installed you will now need to run Juggernaut. Juggernaut serves as an
interface between the frontend of the application and the backend.

Finally before you can use the included runner you will need to install gevent.
Gevent is a coroutine-based module for concurrency. Here it serves the simple
task of reading off a file without blocking execution:



At this point you should be able to execute the runner script and point your
browser to

`http://127.0.0.1:5001`.

## Usage

Once set up, ensure the Juggernaut server is already running and then simply
execute the runner script:

    $ python runner.py
