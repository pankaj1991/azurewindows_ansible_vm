
class Config(object):
    SITE_NAME = 'ANSIBLE PLAYBOOK LOGGER'
    SITE_DOMAIN = 'localhost'

    # indicates the file to watch
    LOG_FILE = '../../azurewindows_ansible_vm/azure-ansible-playbooks/logs/ansible.log'
