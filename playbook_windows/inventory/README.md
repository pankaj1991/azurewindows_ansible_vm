
# Running the Ansible playbook

The script uses ansible playbook to launch azure windows instance using flask applications

`●  Configure a basic (and brand new) Windows 10 Enterprise system for Ansible access `
`●  Install Chocolatey Windows Package Manager using Ansible `
`●  Configure and enable SSH on standard port so that it starts up on restart of machine `
`●  Upload a configurable SSH key (I should be able to set my key so that I can test this` process) 
`●  Install Git using Chocolatey `

(Pre-requisites): Manual Method to launch System

1. WinRM Python client on ansible host -
`pip install pywinrm`

2. WinRM Server Installation on target:
# Set script execution capability in PS -

```
Set-ExecutionPolicy Bypass -scope Process -Force
Set-ExecutionPolicy RemoteSigned
```
OR

```
@powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((new-object net.webclient).DownloadString('https://github.com/ansible/ansible/raw/devel/examples/scripts/ConfigureRemotingForAnsible.ps1'))"
```

# Run the script winrm_installer.ps1 -
`.\winrm_installer.ps1`

`SECOND METHOD`

#(Prerequisites): Launch using Ansible

#Install azure_preview_module role's dependencies packages.
`pip install pywinrm`

`pip install ansible[azure]`

`ansible-galaxy install azure.azure_preview_modules`

`pip install -r ~/.ansible/roles/azure.azure_preview_modules/files/requirements-azure.txt`

`pip install azure-cli`

GET AZURE SUBSCRIPTION ID
`subscription_id=xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx`
`client_id=xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx`
`secret=xxxxxxxxxxxxxxxxx`
`tenant=xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx`

METHOD TO GET AZURE SUBSCRIPTION ID

```az login
az account list --output table
Name        CloudName    SubscriptionId                        State    IsDefault
----------  -----------  ------------------------------------  -------  -----------
Free Trial  AzureCloud   d02ff154-077d-4e20-9793-f81346ea8e1a  Enabled  True
sh-3.2# az account list
[
  {
    "cloudName": "AzureCloud",
    "id": "d02ff154-077d-4e20-9793-f81346ea8e1a",
    "isDefault": true,
    "name": "Free Trial",
    "state": "Enabled",
    "tenantId": "803c8238-cb04-46f7-8319-fd3334dd71ab",
    "user": {
      "name": "coolpankaj.1183@gmail.com",
      "type": "user"
    }
  }
]
sh-3.2#  az account set --subscription="d02ff154-077d-4e20-9793-f81346ea8e1a"
sh-3.2# az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/d02ff154-077d-4e20-9793-f81346ea8e1a"
Creating a role assignment under the scope of "/subscriptions/d02ff154-077d-4e20-9793-f81346ea8e1a"
  Retrying role assignment creation: 1/36
  Retrying role assignment creation: 2/36
  Retrying role assignment creation: 3/36
  Retrying role assignment creation: 4/36
{
  "appId": "3df1ad87-a0e0-46e9-8dcd-f35b545125fa",
  "displayName": "azure-cli-2019-10-25-14-41-40",
  "name": "http://azure-cli-2019-10-25-14-41-40",
  "password": "5eb0e608-7d9b-4b6f-bb32-159b289a0b06",
  "tenant": "803c8238-cb04-46f7-8319-fd3334dd71ab"
}
appId is the client_id defined above.
password is the client_secret defined above.
tenant is the tenant_id defined above.
```

`pip install -I azure-nspkg`


This Uses Manual Method to Run Playbook but recommended is flask
```
ansible-playbook azure_create_windows_vm.yml -e "resource_group_name=test-rg" -  -e "resource_vm_name=[machine name]" -e "resource_username=[username]" -e "resource_password=[password]"
```

Create HOST File in path inventory/
```
[azure]
XXXXXX

[azure:vars]
ansible_connection=winrm
ansible_user=XXXX
ansible_password=XXXX
ansible_remote_tmp=%TEMP%
become_method=runas
ansible_winrm_server_cert_validation=ignore
```

# Execute Playbook for Packages

```
ansible-playbook -i inventory/hosts win_choco.yml
```

