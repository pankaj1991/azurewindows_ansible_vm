#!/bin/bash
t=`date +"%s"`
echo "[azure]" >>  ../playbook_windows/inventory/$2$t
echo $1 >>  ../playbook_windows/inventory/$2$t
echo "" >> ../playbook_windows/inventory/$2$t
echo  "[azure:vars]" >>  ../playbook_windows/inventory/$2$t
echo  "ansible_connection=winrm" >>  ../playbook_windows/inventory/$2$t
echo  "ansible_user=$2" >> ../playbook_windows/inventory/$2$t
echo  "ansible_password=$3" >>  ../playbook_windows/inventory/$2$t
#echo  "ansible_remote_tmp=%TEMP%" >>  /Users/immortal/Documents/Ansible_windows/azurewindows_ansible_vm/playbook_windows/inventory/$2$t
echo  "become_method=runas" >>  ../playbook_windows/inventory/$2$t
echo  "ansible_winrm_server_cert_validation=ignore" >>  ../playbook_windows/inventory/$2$t
echo "********** VM INSTALLATION STARTED FOR $1 *******" >> ../azure-ansible-playbooks/logs/ansible.log 2>&1
/usr/bin/nohup /usr/local/bin/ansible-playbook  -i ../playbook_windows/inventory/$2$t ../playbook_windows/win_choco.yml  >> ../azure-ansible-playbooks/logs/ansible.log 2>&1 &
