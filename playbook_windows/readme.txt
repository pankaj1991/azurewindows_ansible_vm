
# Running the Ansible playbook

(Pre-requisites): Manual Method to launch System

1. WinRM Python client on ansible host -
`pip install pywinrm`

2. WinRM Server Installation on target:
# Set script execution capability in PS -

```
Set-ExecutionPolicy Bypass -scope Process -Force
Set-ExecutionPolicy RemoteSigned
```
OR

```
@powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((new-object net.webclient).DownloadString('https://github.com/ansible/ansible/raw/devel/examples/scripts/ConfigureRemotingForAnsible.ps1'))"
```

# Run the script winrm_installer.ps1 -
`.\winrm_installer.ps1`


#(Prerequisites): Launch using Ansible

#Install azure_preview_module role's dependencies packages.

`pip install ansible[azure]`

`ansible-galaxy install azure.azure_preview_modules`

`pip install -r ~/.ansible/roles/azure.azure_preview_modules/files/requirements-azure.txt`

`pip install azure-cli`

subscription_id=xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
client_id=xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
secret=xxxxxxxxxxxxxxxxx
tenant=xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx

`pip install -I azure-nspkg`


```
ansible-playbook azure_create_windows_vm.yml -e "resource_group_name=test-rg"
```


# Execute Playbook for Packages

```
ansible-playbook -i inventory/hosts win_choco.yml
```
