# WEBUI Launcher

#PROJECT STRUCTURE :
```
Webui_launcher
├── 1
├── LICENSE
├── README.md
├── Resources
│   ├── WTForms-field-types.csv
│   └── WTForms-validators.csv
├── data.pyc
├── requirements.txt
├── static
│   └── css
│       └── main.css
├── templates
│   ├── 404.html
│   ├── 500.html
│   ├── index
│   ├── index.html
│   ├── soft
│   └── soft.html
└── web.py

4 directories, 15 files
```

I have used Python Flask as a frond end Application that allow you to deploy Windows 10 Enterprise over the fly without any manual intervation. The form allow you to give your machine Name,Username and Admin Password that will allow you to do RDP. The front end invokes the Ansible Playbook in backend Named ` azure_create_windows_vm.yml`  with the required roles ,sizing capacity,region and with public access. As a result the Ansible playbook make sures to launch the new instance with all required parameters and prerequisite to do SSH as well as WINRM protocol


```
REPOSITORY :
LOGGER FLASK  : https://bitbucket.org/pankaj1991/azurewindows_ansible_vm/src/master/WebUiLogger/
WEb GUI FLASK FLASK : https://bitbucket.org/pankaj1991/azurewindows_ansible_vm/src/master/Webui_launcher/
ANSIBLE PLAYBOOK TO LAUNCH INSTANCE : https://bitbucket.org/pankaj1991/azurewindows_ansible_vm/src/master/azure-ansible-playbooks/
ANSIBLE PLAYBOOK TO INSTALL SOFTWARES INSTANCE : https://bitbucket.org/pankaj1991/azurewindows_ansible_vm/src/master/playbook_windows/
```

INSTALLATION STEPS :

```
git clone https://pankaj1991@bitbucket.org/pankaj1991/azurewindows_ansible_vm.git
```

For starting the FLASK WEB LAUNCHER to Launch the instances using GUI

For starting the FLASK WEB LAUNCHER to Launch the instances using GUI

```
$ cd Webui_launcher
$ pip install -r requirement.txt
(If you get error use --upgrade --force-reinstall for dependency)
$ python web.py
```

Open the browser to launch thee below address

Link to Provision New Instance : `127.0.0.1:5000`
Link to Configure New Instance : `127.0.0.1:5000/soft`


GOING STEP BY STEP :



Next Step is to Start Launcher for the LOGS. Assuming you have cloned the repo
```

cd ../WebUiLogger
pip install -r requirement.txt
(If you get error use --upgrade --force-reinstall for dependency)
```

Install all required dependency like redis-server and start using

`python runner.py`

Open the browser  tocheck  LOGS

`127.0.0.1:5001`





A first version of the app is in **web.py**. To run it, it is assumed that you:

1. Are using Python 3.x or 2.7x.

2. Have already installed Flask and its dependencies.

3. Have activated your virtualenv (if you are using one).

Run the app by entering its folder in Terminal and typing:
```
$ cd Webui_launcher
$ pip install -r requirement.txt
(If you get error use --upgrade --force-reinstall for dependency)
$ python web.py
```

Then open your web browser and type this in the address bar:

Link to Provision New Instance : `127.0.0.1:5000`
Link to Configure New Instance : `127.0.0.1:5000/soft`
