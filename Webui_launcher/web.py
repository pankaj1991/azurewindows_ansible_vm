# using python 3
import sh
from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField,validators
from wtforms.validators import Required
import subprocess
from subprocess import Popen, PIPE
from subprocess import check_output


app = Flask(__name__)
# Flask-WTF requires an enryption key - the string can be anything
app.config['SECRET_KEY'] = 'some?bamboozle#string-foobar'
# Flask-Bootstrap requires this line
Bootstrap(app)
# this turns file-serving to static, using Bootstrap files installed in env
# instead of using a CDN
app.config['BOOTSTRAP_SERVE_LOCAL'] = True

# with Flask-WTF, each web form is represented by a class
# "NameForm" can change; "(FlaskForm)" cannot
# see the route for "/" and "index"
class NameForm(FlaskForm):
    name = StringField('Please provide the Windows System Name for Azure?', [validators.Length(min=3, max=15)])
    username = StringField('Please provide the Windows User Name for Azure?',[validators.Length(min=5, max=24)])
    password = PasswordField('Please provide the Windows Password  for Azure?',[validators.Length(min=9, max=27)])
    submit = SubmitField('Lets Deploy')

class Soft(FlaskForm):
    name = StringField('Please provide the Windows IP for Azure to Install?', [validators.Length(min=3, max=24)])
    username = StringField('Please provide the Windows User Name for Azure?',[validators.Length(min=5, max=24)])
    password = PasswordField('Please provide the Windows Password  for Azure?',[validators.Length(min=9, max=27)])
    submit = SubmitField('Lets Install')

# define functions to be used by the routes (just one here)

# retrieve all the names from the dataset and put them into a list

# all Flask routes below

# two decorators using the same function
@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    # you must tell the variable 'form' what you named the class, above
    # 'form' is the variable name used in this template: index.html
    form = NameForm()
    message = ""
    #s3 = sh.bash.bake("az --help")
    #print s3.returncode
    #ax = sh.az("--help")
    if form.validate_on_submit():
        name = form.name.data
        username = form.username.data
        password = form.password.data
        ax = sh.bash("../azure-ansible-playbooks/create.sh" , name.lower() , username , password)
        print(ax)
        message = "Yay! Deployment is on way. You can check your progress on this link <a href='127.0.0.1:5001'>Logs</a>"
        form.name.data=""
        form.username.data=""
        form.password.data=""
    else:
        message = ""
    # notice that we don't need to pass name or names to the template
    return render_template('index.html', form=form, message=message)

@app.route('/soft', methods=['GET', 'POST'])
def soft():
    # you must tell the variable 'form' what you named the class, above
    # 'form' is the variable name used in this template: index.html
    form = Soft()
    message = ""
    #s3 = sh.bash.bake("az --help")
    #print s3.returncode
    #ax = sh.az("--help")
    if form.validate_on_submit():
        name = form.name.data
        username = form.username.data
        password = form.password.data
        ax = sh.bash("../playbook_windows/install.sh" , name.lower() , username , password)
        print(ax)
        message = "Yay! installation is on way. You can check your progress on this link <a href='127.0.0.1:5001'>Logs</a>"
        form.name.data=""
        form.username.data=""
        form.password.data=""
    else:
        message = ""
    # notice that we don't need to pass name or names to the template
    return render_template('soft', form=form, message=message)


# keep this as is
if __name__ == '__main__':
    app.run(debug=True)
